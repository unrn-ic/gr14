def clasica():
    print("\n*Calculadora Clásica*\n")
    operacion=input(" Que Operación va a realizar? \t")
    if operacion=="+":
        acum=0
        print("Valores A Sumar: \n")
        dato=input()
        while str(dato) !="=":
            acum += int(dato)
            dato=input()
        print(acum)
    elif operacion=="-":
        acum=0
        print("Valores A Restar: \n")
        dato=input()
        acum=int(dato)
        dato=input()
        while str(dato) !="=":
            acum =acum-int(dato)
            dato=input()
        print(acum)
    elif (operacion=="*"):
        acum=1
        print("Valores A Multiplicar: \n")
        dato=input()
        while str(dato) !="=":
            acum =acum*int(dato)
            dato=input()
        print(acum)
    elif (operacion=="/"):
        acum=1
        print("Valores A Dividir: \n")
        dato=input()
        acum=int(dato)
        while str(dato) !="=":
            aux=input()
            while aux== "0":
                aux=input()
            if aux =="=":
                dato="="
            else:
                acum=acum/int(aux)      
        print(acum)

def fracciones():
    print("\n*Calculadora de Fracciones*\n")
    operacion=input(" Que Operación realizar? \t")
    print("Instrucciones:")
    print("Para salir, presione = cuando se le pida numerador")
    print("1RO Ingrese numerador")
    print("2DO Ingrese denominador\n")
    if operacion=="+":
        aux1=0
        aux2=1
        numerador=(input("numerador "))
        while (numerador!="="):
            denominador1=0
            while denominador1==0:
                denominador1=int(input("denominador "))
            numerador1=int(numerador)
            aux1=(denominador1*aux1)+(numerador1*aux2)
            aux2*=denominador1
            numerador=(input("numerador "))
        if aux1==0:
            print(aux1)
        else:    
            print("\t", aux1,"/",aux2)
        
    elif operacion=="*":
        aux1=1
        aux2=1
        numerador=(input("numerador "))
        while (numerador!="="):
            denominador1=0
            while denominador1==0:
                denominador1=int(input("denominador "))
            numerador1=int(numerador)
            aux1*=numerador1
            aux2*=denominador1
            numerador=(input("numerador "))
        print(aux1,"/",aux2)
    elif operacion=="/":
        aux1=0
        aux2=0
        contador=0
        numerador=(input("numerador "))
        while (numerador!="="):
            denominador1=0
            while denominador1==0:
                denominador1=int(input("denominador "))
            numerador1=int(numerador)
            contador+=1
            if contador==1:
                aux1=numerador1
                aux2=denominador1
            else:
                aux1=aux1*denominador1
                aux2=aux2*numerador1
            numerador=(input("numerador ")) 
        if aux1==0:
            print(aux1)
        else:    
            print("\t", aux1,"/",aux2)   
                
    elif operacion=="-":
        aux1 =0
        aux2=1
        contador=0
        numerador=(input("numerador "))
        while (numerador!="="):
            denominador1=0
            while denominador1==0:
                denominador1=int(input("denominador "))
            numerador1=int(numerador)
            contador+=1
            if contador==1:
                aux1=numerador1
                aux2=denominador1
            else:
                aux1=(denominador1*aux1)-(numerador1*aux2)
                aux2*=denominador1
            numerador=(input("numerador "))
        if aux1==0:
            print(aux1)
        else:    
            print("\t", aux1,"/",aux2)

    
def conversiones(numero,base):
    if base=="bin":
        acumulador=" "  
       
        while numero>=1:
            mod=numero%2
            numero=numero//2
            mods=str(mod)
            acumulador=mods+acumulador
        print(acumulador)
    elif base=="octal":
        acumulador=" "
        while (numero>=1):
            mod=numero%8
            numero=numero//8
            mods=str(mod)
            acumulador=mods+acumulador
        print(acumulador)
    elif base=="hex":
        if numero<16:
            valor=str(numero)
            lista={"10": "a","11": "b","12": "c","13": "d","14": "e","15": "f",}
            if valor in lista:
                print (lista[valor])
            elif numero<10:
                print(numero)
        else:
            hexadecimal = ""
            while numero > 0:
                residuo = numero% 16
                residuo1=str(residuo)
                lista={"10": "a","11": "b","12": "c","13": "d","14": "e","15": "f",}
                if residuo1 in lista:
                    caracter=lista[residuo1]
                else:
                    caracter=residuo1
                hexadecimal =caracter + hexadecimal
                numero=numero//16
        print(hexadecimal)
    else:
        ("base incorrecta")

encendido=0
opcion=""

while encendido != "on":
    encendido=input("Encienda la calculadora?(on)")
    encendido=str.upper(encendido)
print("A encendido la calculadora!")
while opcion!="4":
    opcion=input("menu de opciones: 1) calculadora clasica\n 2) calculadora de fracciones\n 3)calculadora de conversiones\n 4)salir\n")
    if opcion=="1":
        clasica()
    elif opcion=="2":
        fracciones()
    elif opcion=="3":
        numero=-1
        while (numero<=0) or (numero>9999):
            numero=int(input("ingrese el numero para hacer la conversion"))
            print("bin       hex     octal")
        base=input("A que base quiere convertir?")
        base=str.upper(base)
        conversiones(numero,base)
    else:
        print("\nUsted a salido ")